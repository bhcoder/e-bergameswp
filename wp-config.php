<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'e-bergameswp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B[dwihmjs@=.l0e j4!, ^@f5<YO:sZD_)]WCTNd%Y3o|Hq(W,q:2 B9{TQI7L!G');
define('SECURE_AUTH_KEY',  'yo|k`yCGSP 9~LH@.0GyRjRo()lq=Vr/3:y](n&;Z|+XqB*CO~CK_sVq#4bDMjO9');
define('LOGGED_IN_KEY',    'd7nl9&wemfXjXLpv,&^b!>&%1wEIt*-JPDN%^9K 5HK-zB|?N5xLG0A,~{XHd%)@');
define('NONCE_KEY',        'qMm}ELTHdTRz(+CAta`{ZOY/D1,;3Q?I0 m3s0s{V>mluJ[x?{/t+r7zeUi^Oj_A');
define('AUTH_SALT',        'J+?c/Q:4n[WU@DC$I^(%.+>b$hPM[$i/tVU?VRMAii$dt5$BK!Q:Rfww9*?SfTf,');
define('SECURE_AUTH_SALT', '-pnO?O7%00KJSthX~SZ0^7Oe!V:[)mYy0&b.;X5Yo>(>qvEuP4$ywE!UXG>c1mCU');
define('LOGGED_IN_SALT',   'NYXGPXcygmmtez}PvG=KhiLW57~m/t}n&_zQ>MxCgnbnAZ>eK{hpGp7t,VRtjteO');
define('NONCE_SALT',       'In)/<wF]}-;7U#.ikUa8#eXcq AZ-d|C(*pr;]WW&S7PNcS@u|x;< 7==UQV6faZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
